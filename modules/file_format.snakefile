#!/usr/bin/env python

# vim: syntax=python tabstop=4 expandtab
# coding: utf-8

#------------------------------------
# @author: Mahesh Vangala
# @email: vangalamaheshh@gmail.com
# @date: July, 1st, 2016
#------------------------------------

rule get_chrom_size:
    output:
        "analysis/bam2bw/" + config['reference'] + ".chrom.sizes"
    params:
        config['chrom_size_file']
    message: "Fetching chromosome sizes"
    shell:
        "mkdir -p analysis/bam2bw && cp {params} {output}"
        #" && if [ -e /zfs/cores/mbcf/mbcf-storage/devel/umv/ref_files/ERCC/input/ERCC92.chromInfo ]; then"
        #" cat /zfs/cores/mbcf/mbcf-storage/devel/umv/ref_files/ERCC/input/ERCC92.chromInfo 1>>{output}; fi"


rule bam_to_bigwig:
    input:
        bam="analysis/STAR/{sample}/{sample}.sorted.bam",
        chrom_size="analysis/bam2bw/" + config['reference'] + ".chrom.sizes"
    output:
        "analysis/bam2bw/{sample}/{sample}.bw"
    params:
        "analysis/bam2bw/{sample}/{sample}"
    message: "Converting {wildcards.sample} bam to bigwig"
    benchmark:
        "benchmarks/{sample}/{sample}.bam_to_bigwig.txt"
    shell:
        "bedtools genomecov -bg -split -ibam {input.bam} 1> {params}.bg"
        " && bedSort {params}.bg {params}.sorted.bg"
        " && bedGraphToBigWig {params}.sorted.bg {input.chrom_size} {output}"


