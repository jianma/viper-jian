#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: martin
# @Date:   2018-07-05 10:12:50
# @Last Modified by:   martin
# @Last Modified time: 2018-07-05 10:12:50

'''Usage: python merge_cufflinks_fpkm.py input1 input2 ... input_n output
the last argument is output
'''

import sys
import io
import types


def read_table_deco(ins):
    '''Decorator for convert filename to filehandler,
    return None if file not exists
    '''
    def wrapper(table_file, *args, **kwargs):
        if isinstance(table_file, io.TextIOBase):
            inf = table_file
        else:
            try:
                inf = open(table_file)
            except IOError:
                print("no such file: {}".format(table_file))
                return None
        return ins(inf, *args, **kwargs)
    return wrapper


@read_table_deco
def read_table(table_file, sep="\t", skip='\x00', process_func=None):
    """read_table(table_file, sep="\t", skip="\x00", process_func=None)
    skip the lines start with \x00.
    """
    skipcount = 0
    for line in table_file:
        line = line.rstrip("\r\n")
        if line.startswith(skip):
            skipcount += 1
            continue
        if sep.lower() == "space":
            line_split = line.split()
        else:
            line_split = line.split(sep)
        if process_func is not None:
            line_split = process_func(line_split)
        yield line_split
    table_file.close()
    if skipcount:
        print('Skip {count} lines while reading file.'.format(count=skipcount))


# file formated dataoutput (list/dict)
def write_table(l, filename, sep='\t'):
    """write_table(l,filename,RowNames='F') -> None
    """
    with open(filename, 'w') as outf:
        if type(l) in (list, tuple, types.GeneratorType):
            for l1 in l:
                if type(l1) in (list, tuple, types.GeneratorType):
                    #writer = csv.DictWriter(outf, delimiter=sep)
                    #writer.writerows(l)
                    k = (str(t) for t in l1)
                    outf.writelines(sep.join(k) + '\n')
                else:
                    outf.write('%s\n' % l1)
        elif type(l) == dict:
            for k in l:
                if type(l[k]) in (list, tuple, types.GeneratorType):
                    outf.write(sep.join([k] + [str(u) for u in l[k]])+'\n')
                else:
                    outf.write('%s%s%s\n' % (k, sep, l[k]))


def main():
    inputs = sys.argv[1:-1]
    output = sys.argv[-1]
    out = {}
    all_genes = []
    all_tags = []
    for input in inputs:
        tag = input.split('/')[-1]
        all_tags.append(tag)
        data = read_table(input)
        #head = next(data)
        gene_c = 0  # head.index('gene_id')
        fpkm_c = 1  # head.index('FPKM')
        for line in data:
            gene = line[gene_c]
            fpkm = line[fpkm_c]
            all_genes.append(gene)
            try:
                out[gene][tag] = fpkm
            except KeyError:
                out[gene] = {}
                out[gene][tag] = fpkm
    all_genes = sorted(list(set(all_genes)))
    out2 = [[g] + [out[g].get(t, 0) for t in all_tags] for g in all_genes]
    out2.insert(0, ['Gene'] + all_tags)
    write_table(out2, output, sep=',')


if __name__ == '__main__':
    main()

