#!/usr/bin/env Rscript
# @Author: martin
# @Date:   2020-03-06 16:05:01
# @Last Modified by:   martin
# @Last Modified time: 2020-03-06 16:31:06
suppressMessages(library("dplyr"))
suppressMessages(library("AnnotationDbi"))
suppressMessages(library("gage"))
suppressMessages(library("gageData"))
suppressMessages(library("pathview"))
suppressWarnings(suppressMessages(library("clusterProfiler")))
suppressMessages(library("XML"))
suppressMessages(library("ggplot2"))
suppressMessages(library("stringr"))


args <- commandArgs(trailingOnly=TRUE)
deseq_file      <- args[1]
numkeggpathways <- as.numeric(args[2])
kegg_dir        <- args[3]
genome_assembly <- args[4]
temp_dir        <- args[5]
kegg_table_up   <- args[6]
kegg_table_down <- args[7]
keggsummary_pdf <- args[8]
up_kegg_png     <- args[9]
down_kegg_png   <- args[10]
gsea_table      <- args[11]
gsea_pdf        <- args[12]

genome_assembly2 = substr(genome_assembly, 1, 2)  # hg, mm
if (genome_assembly2 == 'hg') {
    species <- 'hsa'
    suppressMessages(library("org.Hs.eg.db"))
} else if (genome_assembly2 == 'mm') {
    species <- 'mmu'
    suppressMessages(library("org.Mm.eg.db"))
}
kegg_data_dir = paste0('/static_data/kegg.jp/pathway/', species, '_files')

## The traceback is actually necessary to not break pipe at the stop step, so leave on
options(error = function() traceback(2))

## Example inputs
#deseq_file = "/mnt/cfce-stor1/home/mgc31/code/viperproject/analysis/test_all/diffexp/BCellvsTCell/BCellvsTCell.deseq.csv"
#keggpvalcutoff = 0.1
#numkeggpathways = 5
#reference = '' #"hg19"
#kegg_dir = "/mnt/cfce-stor1/home/mgc31/code/viperproject/analysis/test_all/diffexp/BCellvsTCell/kegg_pathways/"
#temp_dir = "/mnt/cfce-stor1/home/mgc31/code/viperproject/analysis/test_all/diffexp/BCellvsTCell/temp/"
#kegg_table_up = "/mnt/cfce-stor1/home/mgc31/code/viperproject/uptest.csv"
#kegg_table_down = "/mnt/cfce-stor1/home/mgc31/code/viperproject/downtest.csv"
#keggsummary_pdf = "/mnt/cfce-stor1/home/mgc31/code/viperproject/test.pdf"
#up_kegg_png = "/mnt/cfce-stor1/home/mgc31/code/viperproject/upkeggtest.png"
#down_kegg_png = "/mnt/cfce-stor1/home/mgc31/code/viperproject/downkeggtest.png"
#gsea_table = "/mnt/cfce-stor1/home/mgc31/code/viperproject/gseatest.csv"
#gsea_pdf = "/mnt/cfce-stor1/home/mgc31/code/viperproject/gseatest.pdf"


kegg_pathway_f <- function(deseq_file, numkeggpathways, kegg_dir, species, temp_dir, kegg_table_up, kegg_table_down, keggsumary_pdf, up_kegg_png, down_kegg_png, gsea_table,gsea_pdf){
    # species: hsa or mmu

    ## Will need this path stuff for later as kegg output is very messy
    #mainDir = substr(kegg_dir, 1, nchar(kegg_dir)-14)
    dir.create(kegg_dir, showWarnings = FALSE)


    kegg.set = kegg.gsets(species=species, check.new=F)  # Failed to connect to rest.kegg.jp port 80: Connection timed out
    ks = kegg.set$kg.sets
    kss = kegg.set$kg.sets[kegg.set$sigmet.idx]
    names(kss) = gsub("/", "%2F", names(kss))

    ## Removing pathways that I know don't load properly... no idea why
    #pathway_errors = c("hsa01200 Carbon metabolism", "hsa01230 Biosynthesis of amino acids", "hsa01212 Fatty acid metabolism", 
    #    "hsa01210 2-Oxocarboxylic acid metabolism", "hsa01100 Metabolic pathways", "hsa00533 Glycosaminoglycan biosynthesis - keratan sulfate", 
    #    "hsa00514 Other types of O-glycan biosynthesis", "hsa00511 Other glycan degradation")
    #kss[which(names(kss) %in% pathway_errors)] <- NULL


    ## Read in deseq table
    detable = read.table(deseq_file, header=TRUE, sep=",", fill=TRUE)
    rownames(detable) <- detable[, 'id']

    ## Append ENSEMBL and ENTREZ IDs from loaded in db
    if (species == 'hsa') {
        IDdb = org.Hs.eg.db
    } else if (species == 'mmu') {
        IDdb = org.Mm.eg.db
    }
    # map symbol to entrez ID
    detable$entrez = mapIds(IDdb,
                            keys=row.names(detable),
                            column="ENTREZID",
                            keytype="SYMBOL",
                            multiVals="first")
    ## Couple failsafes
    detable = na.omit(detable)
    detable = detable[is.finite(detable$log2FoldChange), ]
    
    ## Setting up gage input, needs the log2fc with the entrez id
    gageinput = detable$log2FoldChange
    names(gageinput) = detable$entrez

    # Run gage
    # names(keggres)
    # [1] "greater" "less"    "stats"
    keggres = gage(gageinput, gsets = kss, same.dir=TRUE)

    ## Output kegg results with respect to upregulation
    kegg_up = keggres$greater
    kegg_up = cbind(rownames(kegg_up), kegg_up)
    colnames(kegg_up)[1] = "Kegg_pathway"
    kegg_up[,1] = gsub(",", "%2C", as.matrix(kegg_up[,1]))
    write.table(kegg_up, file = kegg_table_up, quote=F, col.names=TRUE, row.names=FALSE, sep=",")

    ## Output kegg results with respect to downregulation
    kegg_down= keggres$less
    kegg_down= cbind(rownames(kegg_down), kegg_down)
    colnames(kegg_down)[1] = "Kegg_pathway"
    kegg_down[,1] = gsub(",", "%2C", as.matrix(kegg_down[,1]))
    write.table(kegg_down, file = kegg_table_down, quote=F, col.names=TRUE, row.names=FALSE, sep=",")

    ## Get the pathways
    #keggrespathways = keggres$stats
    #keggrespathways = keggrespathways[order(keggrespathways[, 'stat.mean'], decreasing=T), ]
    #keggrespathways = rownames(keggrespathways)[1:numkeggpathways]
    #keggresids = substr(keggrespathways, start=1, stop=8)
    keggresids_up = substr(rownames(kegg_up)[1:numkeggpathways], start=1, stop=8)
    keggresids_down = substr(rownames(kegg_down)[1:numkeggpathways], start=1, stop=8)


    keggresids_all = c(keggresids_up, keggresids_down)
    ## Plot using pathview
    normwd = getwd()
    setwd(temp_dir)
    for(kegg_id in keggresids_all){
        xml_f <- file.path(kegg_data_dir, paste0(kegg_id, '.xml'))
        png_f <- file.path(kegg_data_dir, paste0(kegg_id, '.png'))
        if (!file.exists(xml_f)) {
            cat(paste0('Downloading: http://rest.kegg.jp/get/', kegg_id, '/kgml'))
            download.file(paste0('http://rest.kegg.jp/get/', kegg_id, '/kgml'), xml_f)
        }
        if (!file.exists(png_f)) {
            cat(paste0('Downloading: http://rest.kegg.jp/get/', kegg_id, '/kgml'))
            download.file(paste0('http://rest.kegg.jp/get/', kegg_id, '/image'), png_f)
        }
        file.copy(xml_f, paste0(kegg_id, '.xml'))
        file.copy(png_f, paste0(kegg_id, '.png'))

        suppressMessages(pvout <- pathview(gene.data = gageinput,              ## Gene list
                          pathway.id = kegg_id,      ## Which pathway
                          species = species,                ## Species
                          #limit = list(gene=max(abs(gageinput)),cpd=1),
                          #kegg.dir = temp_dir              ## Save directory
                          low = list(gene = "blue", cpd = "purple"),
                          mid = list(gene = "gray", spd = "gray"),
                          high = list(gene = "red", cpd = "gray")  ## Color scale
        ))
    }
    setwd(normwd)

    ## Renaming files
    # Create variable with keggrespathways sorted and pull out name
    ##sortkeggrespathways = sort(keggrespathways)
    ##if (species == 'hsa') {
    ##    newnames = substr(sortkeggrespathways, 10, nchar(sortkeggrespathways))
    ##
    ##    newnames = gsub(" ", "_", newnames)
    ##    newnames = paste0(newnames, "_", match(sortkeggrespathways, keggrespathways))
    ##}
    # Read in the list of made png files
    png_files <- list.files(temp_dir, pattern=glob2rx("*.pathview.png"))
    xml_files <- list.files(temp_dir, pattern=glob2rx("*.xml"))
    file.copy(file.path(temp_dir, png_files), file.path(kegg_dir, png_files))
    file.copy(file.path(temp_dir, xml_files), file.path(kegg_dir, xml_files))

    ## Create KEGG Summary Tables for up and downregulated pathways
    plot_list = list()
    
    ## Create Up KEGG Summary Table
    upvalues = sapply(kegg_up[,5], as.numeric)
    uplogqval = -log(upvalues)
    upkeggsummary = data.frame(Keggpathway=substr(names(uplogqval), 10, nchar(names(uplogqval))), logqval=uplogqval)
    
    ## Create title for plot
    temptitle = tail(unlist(strsplit(keggsummary_pdf, split="/")), n=1)
    temptitle = head(unlist(strsplit(temptitle, split="[.]")), n=1)
    uptitle = paste(temptitle, "_Top_", numkeggpathways, "_Kegg_Pathways_UP", sep="")
    downtitle = paste(temptitle, "_Top_", numkeggpathways, "_Kegg_Pathways_DOWN", sep="")
    
    ## Create Up Plot
    kegg.term.width = 20
    upkegg.df <- as.data.frame(upkeggsummary[numkeggpathways:1,], stringsAsFactors=FALSE)
    to_fold <- which(lapply(as.character((upkegg.df$Keggpathway)), nchar) > kegg.term.width)

    foldednames <- sapply(upkegg.df$Keggpathway[to_fold],function(x) paste(strwrap(x,width = kegg.term.width),collapse = "\n"))
    levels(upkegg.df$Keggpathway) <- c(levels(upkegg.df$Keggpathway), foldednames)
    upkegg.df[to_fold,1] <- foldednames
    
    ggup <- ggplot(upkegg.df, aes(y=reorder(Keggpathway, logqval), x=logqval)) +
        geom_segment(aes(y=reorder(Keggpathway, logqval), yend=reorder(Keggpathway, logqval), x=0, xend=logqval)) +
        geom_point(aes(y=reorder(Keggpathway, logqval), x=logqval), color = "steelblue", size=5) +
        labs(x="- Log(Q-value)", y=NULL, title=uptitle) +
        theme(panel.grid.major.y=element_blank()) +
        theme(panel.grid.minor=element_blank()) +
        theme(axis.text.y=element_text(size=10)) +
        theme(plot.title = element_text(size=8, margin = margin(10, 0, 20, 0)))
    plot_list[[1]] = ggup                                                   
    

    ## Create Down KEGG Summary Table
    downvalues = sapply(kegg_down[,5], as.numeric)
    downlogqval = -log(downvalues)
    downkeggsummary = data.frame(Keggpathway=substr(names(downlogqval), 10, nchar(names(downlogqval))), logqval=downlogqval)

    ## Create Down Plot
    kegg.term.width = 20
    downkegg.df <- as.data.frame(downkeggsummary[numkeggpathways:1,], stringsAsFactors=FALSE)
    to_fold <- which(lapply(as.character((downkegg.df$Keggpathway)), nchar) > kegg.term.width)

    foldednames <- sapply(downkegg.df$Keggpathway[to_fold],function(x) paste(strwrap(x,width = kegg.term.width),collapse = "\n"))
    levels(downkegg.df$Keggpathway) <- c(levels(downkegg.df$Keggpathway), foldednames)
    downkegg.df[to_fold,1] <- foldednames

    ggdown <- ggplot(downkegg.df, aes(y=reorder(Keggpathway, logqval), x=logqval)) +
        geom_segment(aes(y=reorder(Keggpathway, logqval), yend=reorder(Keggpathway, logqval), x=0, xend=logqval)) +
        geom_point(aes(y=reorder(Keggpathway, logqval), x=logqval), color = "steelblue", size=5) +
        labs(x="- Log(Q-value)", y=NULL, title=downtitle) +
        theme(panel.grid.major.y=element_blank()) +
        theme(panel.grid.minor=element_blank()) +
        theme(axis.text.y=element_text(size=10)) +
        theme(plot.title = element_text(size=8, margin = margin(10, 0, 20, 0)))
    plot_list[[2]] = ggdown

    
    pdf(keggsummary_pdf)
    for (i in 1:2) { print(plot_list[[i]]) }
    junk <- dev.off()

    png(up_kegg_png, width = 8, height = 8, unit="in",res=300)
    print(plot_list[[1]])
    junk <- dev.off()

    png(down_kegg_png, width = 8, height = 8, unit="in",res=300)
    print(plot_list[[2]])
    junk <- dev.off()
    # --------------- end of KEGG analysis ---------------


    ## GSEA Analysis
    gseainput = sort(gageinput, decreasing=TRUE)
    gseainput = gseainput[is.finite(gseainput)]
    cat(species)
    fullgsea <- gseKEGG(geneList = gseainput,
                        organism     = species,
                        nPerm        = 100,
                        minGSSize    = 1,
                        pvalueCutoff = 0.99,
                        verbose      = FALSE,
                        use_internal_data = FALSE)
    gsea_data = summary(fullgsea)
    gsea_data = gsea_data[order(-abs(gsea_data$NES)),]
    gsea_data[,2] = gsub(",", "", as.matrix(gsea_data[,2]))
    write.table(gsea_data, file = gsea_table, quote=FALSE, sep= ",", row.names=FALSE, col.names=TRUE)

    pdf(gsea_pdf)
    plot.new()
    mtext("GSEA_plots")
    for ( i in 1:10 ) {
        gseaplot(fullgsea, geneSetID = gsea_data[i,1])
        mtext(gsea_data[i,2])
    }
    junk <- dev.off()
}


kegg_pathway_f(deseq_file, numkeggpathways, kegg_dir, species, temp_dir, kegg_table_up, kegg_table_down, keggsumary_pdf, up_kegg_png, down_kegg_png, gsea_table, gsea_pdf)


