# fix_rsem_gene_id.py
import sys
import func

infile = sys.argv[1]
outfile = sys.argv[2]
sep=','
data = list(func.read_table(infile, sep=sep))
remove_line = None # remove the line start with gene_id in the middle of file
for j, l in enumerate(data):
    if j == 0:
        continue
    if l[0] == 'gene_id':
        remove_line = j
        print('remove line', data[remove_line])
        continue
    gene = l[0]
    gene = gene.split('_')
    seq_count = len(gene)
    new_gene = gene[:seq_count//2]
    new_gene = '_'.join(new_gene)
    l[0] = new_gene
    #for u in range(1, len(l)):
    #    l[u] = round(float(l[u]))

del(data[remove_line])
func.write_table(data, outfile, sep=sep)

