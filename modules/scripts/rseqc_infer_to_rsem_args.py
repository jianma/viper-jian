#rseqc_infer_to_rsem_args.py
import sys

infile, outfile = sys.argv[1:3]

#infile = 'analysis/RSeQC/infer_experiment_FUSCCTNBC359.txt'
#outfile = 'analysis/RSeQC/infer_experiment_FUSCCTNBC359.parse'
out = []
pair = ['', '']  # ++, +-
with open(infile) as inf:
    for line in inf:
        if line.startswith('Fraction of reads'):
            if line.startswith('Fraction of reads failed to determine'):
                v = float(line.split(':')[-1].strip())
                if v > 0.1:
                    out.append('ERROR: too many failed read\n')
            else:
                if '1+-' in line:
                    pair[1] = float(line.split(':')[-1].strip())
                elif '1++' in line:
                    pair[0] = float(line.split(':')[-1].strip())

with open(outfile, 'w') as outf:
    if pair[0] > 0.85:
        sys.stdout.write('--forward-prob=1')
        outf.write('%s --forward-prob=1' % infile)
    elif pair[1] > 0.85:
        sys.stdout.write('--forward-prob=0')
        outf.write('%s --forward-prob=0' % infile)
    elif 0.15 < pair[0] < 0.85 and 0.15 < pair[1] < 0.85:
        sys.stdout.write('--forward-prob=0.5')
        outf.write('%s --forward-prob=0.5' % infile)
    else:
        sys.stdout.write('Parse failed')
        outf.write('%s --forward-prob failed' % infile)

