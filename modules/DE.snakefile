#!/usr/bin/env python

# vim: syntax=python tabstop=4 expandtab
# coding: utf-8

import os
from scripts.utils import _getSTARcounts

rule limma_and_deseq:
    input:
        counts = _getSTARcounts(config)[0]
    output:
        limma = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.limma.csv",
        deseq = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.deseq.csv",
        deseqSum = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.deseq.sum.csv",
        #annotations
        limma_annot = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.limma.annot.csv",
        deseq_annot = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.deseq.annot.csv",
    params:
        s1 = lambda wildcards: ",".join(config['comps'][wildcards.comparison]['control']),
        s2 = lambda wildcards: ",".join(config['comps'][wildcards.comparison]['treat']),
        gene_annotation = os.path.join(config['viper_static_dir'], config['reference'], config['reference']+'.annot.csv.bz2')
    message: "Running differential expression analysis using limma and deseq for {wildcards.comparison}"
    benchmark:
        "benchmarks/" + config["token"] + "/{comparison}.limma_and_deseq.txt"
    shell:
        "Rscript {config[viper_dir]}/modules/scripts/DEseq.R \"{input.counts}\" \"{params.s1}\" \"{params.s2}\" " 
        "{output.limma} {output.deseq} {output.limma_annot} {output.deseq_annot} "
        "{output.deseqSum} {params.gene_annotation} "
        # ridiculous hack for singletons (Mahesh Vangala)
        "&& touch {output.limma} "
        "&& touch {output.limma_annot}"

rule create_deseq_rld_matrix:
    input:
        counts = _getSTARcounts(config)[0]
    output:
        rld_matrix = "analysis/" + config["token"] + "/diffexp/deseq_rld_exprs_matrix.csv",
        rld_matrix_clean = "analysis/" + config["token"] + "/diffexp/deseq_rld_exprs_matrix.protein_coding.csv",
    params:
    message: "Generating deseq rld expression matrix for samples"
    benchmark:
        "benchmarks/" + config["token"] + "/deseq.rld.matrix.txt"
    shell:
        "Rscript {config[viper_dir]}/modules/scripts/create_deseq_rld_exprs_matrix.R {input.counts} {config[metasheet]} {output.rld_matrix} "
        "&& touch {output.rld_matrix}\n"
        "Rscript {config[viper_dir]}/modules/scripts/filter_out_miRNA.R --rpkm_file {output.rld_matrix} --genes {config[genome_lib_dir]}/protein_coding_genes.txt --out_file {output.rld_matrix_clean}"

rule deseq_limma_fc_plot:
    input:
        deseq = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.deseq.csv",
        limma = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.limma.csv"
    output:
        out_csv = "analysis/" + config["token"] + "/diffexp/{comparison}/deseq_limma_fc_corr.csv",
        out_png = "analysis/" + config["token"] + "/diffexp/{comparison}/deseq_limma_fc_corr.png"
    message: "Creatting deseq-limma correlation plot for {wildcards.comparison}"
    benchmark:
        "benchmarks/" + config["token"] + "/{comparison}.deseq_limma_fc_plot.txt"
    shell:
        "Rscript {config[viper_dir]}/modules/scripts/deseq_limma_fc_corr.R {input.deseq} {input.limma} {output.out_csv} {output.out_png}"


rule fetch_DE_gene_list:
    input:
        deseq_file_list=expand("analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.deseq.csv",comparison=config["comparisons"]),
        force_run_upon_meta_change = config['metasheet'],
        force_run_upon_config_change = config['config_file']
    output:
        csv="analysis/" + config["token"] + "/diffexp/de_summary.csv",
        png="analysis/" + config["token"] + "/diffexp/de_summary.png"
    message: "Creating Differential Expression summary"
    params:
        deseq_file_string = lambda wildcards, input: ' -f '.join(input.deseq_file_list)
    benchmark:
        "benchmarks/" + config["token"] + "/fetch_DE_gene_list.txt"
    shell:
        #deseq_file_string = ' -f '.join(input.deseq_file_list)
        "perl {config[viper_dir]}/modules/scripts/get_de_summary_table.pl -f {params.deseq_file_string} 1>{output.csv}\n"
        "Rscript {config[viper_dir]}/modules/scripts/de_summary.R {output.csv} {output.png}"


#Generate volcano plots for each comparison
rule volcano_plot:
    input:
        deseq = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.deseq.csv",
        force_run_upon_meta_change = config['metasheet'],
        force_run_upon_config_change = config['config_file']
    output:
        plot = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}_volcano.pdf",
        png = "analysis/" + config["token"] + "/plots/images/{comparison}_volcano.png"
    message: "Creating volcano plots for Differential Expressions for {wildcards.comparison}"
    benchmark:
        "benchmarks/" + config["token"] + "/{comparison}.volcano_plot.txt"
    shell:
        "Rscript {config[viper_dir]}/modules/scripts/volcano_plot.R {input.deseq} {output.plot} {output.png}"


