from scripts.utils import _getCuffCounts, _getRsemTpm

rule immunology_all:
    input:
        "analysis/"+config["token"]+"/immunology/TIMER_estimate.txt",
        "analysis/"+config["token"]+"/immunology/TIMER_results.pdf",

rule estimate_immune_abundance:
    input:
        #fpkm_collected = _getCuffCounts(config)[1]
        fpkm_collected = _getRsemTpm(config)[0]
    output:
        "analysis/"+config["token"]+"/immunology/TIMER_estimate.txt",
        "analysis/"+config["token"]+"/immunology/TIMER_results.pdf",
    params:
        token = config["token"],
        cancer_type = config["cancer_type"].upper()
    message: "Estimating immune cell abundance output with TIMER"
    benchmark:
        "benchmarks/"+config["token"]+"estimate_immune_abundance.txt"
    shell:
        "Rscript {config[viper_dir]}/modules/scripts/TIMER_estimate.R -f {input.fpkm_collected} -c {params.cancer_type} --staticdir={config[viper_static_dir]}/immunology --outdir=`pwd`/analysis/{params.token}/immunology/"

rule tcga_cibersort:
    input:
        #fpkm_collected = _getCuffCounts(config)[1]
        fpkm_collected = _getRsemTpm(config)[0]
    output:
        "analysis/"+config["token"]+"/immunology/tcga_cibersort.txt",
    params:
        token = config["token"],
        species = config["reference"][:2]
    message: "Running TCGA cibersort"
    benchmark:
        "benchmarks/"+config["token"]+"tcga_cibersort.txt"
    shell:
        "Rscript {config[viper_dir]}/modules/scripts/cibersort.R -f {input.fpkm_collected} --static={config[viper_static_dir]}/immunology/tcga_cibersort/LM22_{params.species}.txt --output=analysis/{params.token}/immunology/tcga_cibersort.txt"

# :set syntax=python
