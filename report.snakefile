#!/usr/bin/env python
# coding: utf-8
# vim: syntax=python tabstop=4 expandtab

'''This rule is only used to generate report if you don't want to run all previous steps of viper.
Usage: copy the file to your working dir and run $ snakemake -p -s report.snakefile
'''

import os
import sys
import yaml
viper_dir = yaml.load(open('config.yaml')).get('viper_dir')
sys.path.insert(0, viper_dir)
sys.path.insert(0, viper_dir+'/modules')


from modules.scripts.config_setup import updateConfig
from modules.scripts.metasheet_setup import updateMeta
from modules.scripts.utils import getTargetInfo

from modules.scripts.viper_report import get_sphinx_report
from snakemake.utils import report
from modules.scripts.utils import getTargetInfo
from modules.scripts.utils import _copyMetaFiles

#---------  CONFIG set up  ---------------
configfile: "config.yaml"   # This makes snakemake load up yaml into config
config = updateConfig(config)
config = updateMeta(config)
#-----------------------------------------

rule target:
    input: getTargetInfo(config), os.path.join("analysis", config["token"], config["token"]+".html")
    message: "Compiling all output"

rule generate_report:
    input:
        getTargetInfo(config),
        force_run_upon_meta_change = config['metasheet'],
        force_run_upon_config_change = config['config_file']
    output:
        "analysis/" + config["token"] + "/" + config["token"] + ".html"
    message: "Generating VIPER report"
    benchmark:
        "benchmarks/" + config["token"] + "/generate_report.txt"
    run:
        sphinx_str = get_sphinx_report(config)
        report(sphinx_str, output[0], metadata="Molecular Biology Core Facilities, DFCI", **{'Copyrights:':config['viper_dir'] + "/mbcf.jpg"})

rule copy_meta_files:
    input:
        config_file = config["config_file"],
        meta_file = config["metasheet"]
    output:
        config_file = _copyMetaFiles(config)[0],
        meta_file = _copyMetaFiles(config)[1]
    message: "Saving config and metasheet into analysis/{config[token]}"
    shell:
        "cp {input.config_file} {output.config_file} && "
        "cp {input.meta_file} {output.meta_file}"

